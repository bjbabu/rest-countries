function restCountries() {
  fetch("https://restcountries.com/v3.1/all")
    .then((res) => res.json())
    .then((data) => {
      return addingCountries(data);
    })
    .then((data) => {
      return addRegion(data);
    })
    .then((data) => {
      let regions = document.querySelectorAll("#region-anchor");
      Array.from(regions).map((region) => {
        region.addEventListener("click", filterRegions);
      });
    })
    .catch((error) => {
      console.error("Error fetching data:", error);
    });
}

// Adding country cards
function addingCountries(countries) {
  const container = document.getElementById("countries");
  countries.map((country) => {
    const newDiv = document.createElement("div");
    newDiv.id = `${country.name.common}`;
    newDiv.className = "country w-64 rounded-md shadow-lg";

    newDiv.innerHTML = `<div id="${country.name.common}-flag" class="object-contain h-40">
                          <img
                            src=${country.flags.png}
                            class="w-full h-full object-fill object-center h-40 rounded-t-md"
                            alt="flag"
                          />
                        </div>
                        <div id="statistics" class="ps-6 pt-6 pb-10">
                          <h2 id="country-name" class="font-bold text-lg pb-3"><span style:>${country.name.common}</span></h2>
                          <ul class="text-sm font-semibold">
                            <li id="population" class="pb-1.5">Population: ${country.population}</li>
                            <li id="${country.region}" class="pb-1">Region: ${country.region}</li>
                            <li id="capital">Capital: ${country.capital}</li>
                          </ul>
                        </div>`;

    container.appendChild(newDiv);
  });

  return countries;
}

// Adding regions in filter
function addRegion(countries) {
  const filterList = document.getElementById("filter-list");
  const regionSet = new Set();

  countries.map((country) => {
    regionSet.add(country.region);
  });

  const sortedRegions = Array.from(regionSet).sort();

  sortedRegions.map((region) => {
    const li = document.createElement("li");
    li.innerHTML = `<a href="#" id="region-anchor">${region}</a>`;

    filterList.appendChild(li);
  });

  return countries;
}

// Toggle filter dropdown
let filterBtnFlag = 0;
let filterBtn = document.getElementById("filter-form");
filterBtn.addEventListener("click", toggleDropDown);

function toggleDropDown() {
  const filter = document.getElementById("filter");

  if (filterBtnFlag === 0) {
    filter.style.display = "block";
    filterBtnFlag = 1;
  } else {
    filter.style.display = "none";
    filterBtnFlag = 0;
  }
}

// Filter countries based on regions
let filterBtnOn = 0;
let filteredCountries = [];
function filterRegions(e) {
  const clickedRegion = e.target.innerText.toLowerCase();
  const filterButton = document.getElementById("filter-btn");
  const filter = document.getElementById("filter");

  filterButton.innerHTML = `${e.target.innerText}<i class="fa-solid fa-chevron-down"></i>`;
  filter.style.display = "none";

  const countries = document.getElementsByClassName("country");

  if (clickedRegion === "filter by region") {
    const countries = document.getElementsByClassName("country");

    Array.from(countries).map((country) => {
      country.style.display = "block";
    });

    filterBtnOn = 0;
    filteredCountries = [];
  } else {
    filterBtnOn = 1;
    filteredCountries = [];
    Array.from(countries).map((country) => {
      const contryRegion =
        country.children[1].children[1].children[1].id.toLowerCase();

      if (contryRegion === clickedRegion) {
        country.style.display = "block";
        filteredCountries.push(country);
      } else {
        country.style.display = "none";
      }
    });
  }
}

restCountries();

// After clicking search button
let searchButton = document.getElementById("search-btn");
searchButton.addEventListener("click", searchCountries);

function searchCountries(e) {
  e.preventDefault();
  const inputCountry = document.getElementById("search-input");

  if (filterBtnOn === 1) {
    Array.from(filteredCountries).map((country) => {
      console.log(country);
      if (country.id.toLowerCase() === inputCountry.value.toLowerCase()) {
        country.style.display = "block";
      } else {
        country.style.display = "none";
      }
    });
  } else {
    const countries = document.getElementsByClassName("country");
    Array.from(countries).map((country) => {
      if (country.id.toLowerCase() === inputCountry.value.toLowerCase()) {
        country.style.display = "block";
      } else {
        country.style.display = "none";
      }
    });
  }
}

// While typing in search bar
let searchBar = document.getElementById("search-input");
searchBar.addEventListener("keyup", searchBarCountries);

function searchBarCountries(e) {
  const inputValue = e.target.value;

  if (filterBtnOn === 1) {
    Array.from(filteredCountries).map((country) => {
      if (country.id.toLowerCase().indexOf(inputValue.toLowerCase()) != -1) {
        country.style.display = "block";
      } else {
        country.style.display = "none";
      }
    });
  } else {
    const countries = document.getElementsByClassName("country");
    Array.from(countries).map((country) => {
      if (country.id.toLowerCase().indexOf(inputValue.toLowerCase()) != -1) {
        country.style.display = "block";
      } else {
        country.style.display = "none";
      }
    });
  }
}
